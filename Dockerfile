#Static content builder
FROM node:latest AS uiBuilder

WORKDIR /app

RUN mkdir bin

COPY . .

RUN node -v

RUN npm install --prefix ./static/ 

RUN npm --prefix ./static/ run build

RUN mv ./static/build/ ./bin/


#Go Lang builder
FROM golang:latest AS serverBuilder

WORKDIR /app

RUN mkdir bin

COPY . .

RUN go mod download

ENV GIN_MODE=release

RUN go build -o ./bin/main.exe cmd/httpd/main.go

#Final image

FROM alpine:3.5

RUN apk --no-cache add ca-certificates

WORKDIR /app/bin

COPY --from=uiBuilder /app/bin .

COPY --from=serverBuilder /app/bin .

RUN ls -ltra

RUN ls -ltra ./build

CMD ["./app/bin/main"]
