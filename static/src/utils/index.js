import React from 'react'
import { Route } from 'react-router-dom'
const RouteWithSubRoutes = (route) => {
    return (
        <Route
            exact
            path={route.path}
            render={props => (
                <route.component {...props} routes={route.routes} />
            )} />
    );
}
class Util {
    static checkForKey(obj, key, defaultValue) {
        if (obj.hasOwnProperty(key)) {
            if (Util.getType(obj[key]) === 'Object') {
                return Object.values(obj).join(',');
            }
            else if (!obj[key].trim().length) {
                return defaultValue;
            }
            else {
                return obj[key];
            }
        }
        return defaultValue;
    }
    static getType(obj) {
        return obj ? obj.constructor.name : null;
    }
}
export { RouteWithSubRoutes, Util }