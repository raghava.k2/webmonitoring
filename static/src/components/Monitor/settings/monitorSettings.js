import React from 'react';
import { Grid, Form, Button, Sidebar, Segment } from 'semantic-ui-react';
import axios from 'axios';

export class MonitoringSetting extends React.Component {
    constructor(props) {
        super(props)
        this.form = { URL: null, IntervalTime: 5, TimeFormat: null };
        this.state = { loading: false, timeFormat: null }
    }
    componentDidMount() {
        const { data } = this.props;
        this.form = { URL: data.URL, TimeFormat: data.IntervalTimeFormat || 'S' }
        this.setState({ TimeFormat: data.IntervalTimeFormat });
    }
    componentDidUpdate(prevProps) {
        const { data: oldData } = prevProps;
        const { data } = this.props;
        if (data.ID !== oldData.ID) {
            this.form = { URL: data.URL, TimeFormat: data.IntervalTimeFormat || 'S' }
            this.setState({ TimeFormat: data.IntervalTimeFormat });
        }
    }
    saveConfig = () => {
        this.setState({ loading: true });
        const { data } = this.props;
        const { ID } = data;
        axios.put(`/api/v1/status/${ID}`, this.form).then(() => {
            this.setState({ loading: false });
        }).catch(() => {
            this.setState({ loading: false });
        })
    }
    handleChange = (event) => {
        this.form.TimeFormat = event.currentTarget.children[0].value;
        this.setState({ timeFormat: event.currentTarget.children[0].value })
    }
    setUrl = (event) => {
        this.form.URL = event.target.value;
    }
    setTime = (event) => {
        this.form.IntervalTime = Number(event.target.value);
    }

    render() {
        const { visible, close, data } = this.props;
        const { loading, timeFormat } = this.state;
        const display = visible ? 'block' : 'none';
        return (
            <Sidebar.Pushable as={Segment} style={{ display }}>
                <Sidebar animation='overlay'
                    icon='labeled'
                    vertical="true"
                    direction='right'
                    visible={visible}
                    width='very wide'
                    style={{ width: 500 }}>
                    <Grid>
                        <Grid.Row columns='2'>
                            <Grid.Column>
                                <Button icon='left arrow' onClick={close} basic>
                                </Button>
                            </Grid.Column>
                            <Grid.Column>
                                <h2>Settings</h2>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                    <Form loading={loading}>
                        <Form.Group widths='sixteen'>
                            <Form.Input fluid label='Url' placeholder='mysite.com' defaultValue={data.URL} name="Url"
                                onChange={this.setUrl} />
                        </Form.Group>
                        <Form.Group inline>
                            <label>Time Format</label>
                            <Form.Radio
                                label='Minutes'
                                value='M'
                                checked={timeFormat === 'M'}
                                onChange={this.handleChange}
                            />
                            <Form.Radio
                                label='Seconds'
                                value='S'
                                checked={timeFormat === 'S'}
                                onChange={this.handleChange}
                            />
                        </Form.Group>
                        <Form.Group widths='sixteen'>
                            <Form.Input type='number' fluid label='Check Interval' placeholder='5 minutes' defaultValue={data.IntervalTime}
                                onChange={this.setTime} />
                        </Form.Group>
                        <Form.Group widths='sixteen' className='action-button'>
                            <Button.Group>
                                <Button onClick={close}>Cancel</Button>
                                <Button.Or />
                                <Button primary onClick={this.saveConfig}>Save</Button>
                            </Button.Group>
                        </Form.Group>
                    </Form>
                </Sidebar>
            </Sidebar.Pushable >
        )
    }
}