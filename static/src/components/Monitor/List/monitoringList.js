import React from 'react';
import { headers } from './monitoringListConstants';
import {
    Table,
    Grid,
    Popup,
    Button
} from 'semantic-ui-react';
import './monitoringList.scss';

export class MonitoringList extends React.Component {

    actionEventsClick = ({ event, data }) => {
        const { currentTarget } = event;
        const type = currentTarget.getAttribute('data-attr');
        const { action } = this.props;
        action({ type, data });
    }

    render() {
        const { rows } = this.props;
        return (
            <Table color='orange'>
                <Table.Header>
                    <Table.Row>
                        {headers.map((head, index) => (
                            <Table.HeaderCell key={index}>{head.label}</Table.HeaderCell>
                        ))}
                    </Table.Row>
                </Table.Header>

                <Table.Body>
                    {rows.map((api, rowIndex) => {
                        return (
                            <Table.Row key={rowIndex}>
                                {headers.map((head, cellIndex) => {
                                    if (head.key === 'URL') {
                                        return (
                                            <Table.Cell className='url-format' key={cellIndex + rowIndex}>
                                                <URL url={api[head.key]}></URL>
                                            </Table.Cell>
                                        )
                                    } else if (head.key === 'actionMenu') {
                                        return (
                                            <Table.Cell key={cellIndex + rowIndex}>
                                                <RowActionMenu actionEvent={this.actionEventsClick} data={api}></RowActionMenu>
                                            </Table.Cell>
                                        )
                                    } else {
                                        return (
                                            <Table.Cell key={cellIndex + rowIndex}>{api[head.key]}</Table.Cell>
                                        )
                                    }
                                })}
                            </Table.Row>
                        )
                    })
                    }
                </Table.Body>
            </Table>
        )
    }
}

const URL = ({ url }) => (
    <div className='url-format'>
        <a href={url} target='_blank' rel='noopener noreferrer'>{url}</a>
    </div>
)

const RowActionMenu = ({ actionEvent, data }) => {
    const action = (event) => {
        actionEvent({ event, data });
    }
    return (
        <Popup trigger={<Button basic icon='ellipsis vertical'></Button>} position='top left' flowing hoverable>
            <Grid centered divided columns={3}>
                <Grid.Column textAlign='center' onClick={action} className='icon' data-attr='delete'>
                    <i className="fas fa-trash-alt"></i>
                </Grid.Column>
                <Grid.Column textAlign='center' onClick={action} className='icon' data-attr='setting'>
                    <i className="fas fa-cog"></i>
                </Grid.Column>
                <Grid.Column textAlign='center' onClick={action} className='icon' data-attr='report'>
                    <i className="fas fa-clipboard"></i>
                </Grid.Column>
            </Grid>
        </Popup >
    );
}