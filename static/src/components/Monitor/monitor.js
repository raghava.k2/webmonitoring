import React from 'react';
import { ResponsiveContainer } from '../NavBar/navBar';
import {
    Segment,
    Container,
    Input,
    Grid,
    List,
    Header,
    Icon,
    Dimmer,
    Loader
} from 'semantic-ui-react';
import axios from 'axios';
import { MonitoringList } from './List/monitoringList';
import { MonitoringReport } from './report/monitorReport';
import { MonitoringSetting } from './settings/monitorSettings';

export class Monitor extends React.Component {

    constructor(props) {
        super(props)
        this.searchRef = null;
        this.state = { rows: [], showReport: false, reportRows: [], showLoading: false, showSetting: false, setting: {} };
    }

    componentDidMount() {
        this.setState({ showLoading: true });
        axios.get('/api/v1/status').then(({ data }) => {
            this.setState({ rows: this.setStatus(data), showLoading: false });
        }).catch(() => {
            this.setState({ showLoading: false });
        })
    }

    fetchUrlStatus = (event) => {
        if (event.which === 13 || event.type === 'click') {
            const { inputRef } = this.searchRef;
            this.setState({ showLoading: true });
            axios.post('/api/v1/status', { url: inputRef.current.value }).then(({ data }) => {
                this.addStatus(data);
                this.setState((state) => {
                    const rows = [...state.rows];
                    rows.unshift(data);
                    return { rows, showLoading: false };
                });
            }).catch(error => {
                console.error(error);
            })
        }
    }

    setSearchRef = (element) => {
        this.searchRef = element;
    }

    setStatus = (apis) => {
        return apis.map(api => {
            this.addStatus(api);
            return api;
        });
    }

    addStatus = (api) => {
        api.Status = (api['TimeTaken'] && api['TimeTaken'].trim()) ? 'Up' : 'Down';
    }

    onHide = () => {
        this.setState({ showReport: false });
    }

    onHideSetting = () => {
        this.setState({ showSetting: false });
        this.componentDidMount();
    }

    handleActions = ({ type, data }) => {
        switch (type) {
            case 'delete':
                this.deleteMonitoringSite(data);
                break;
            case 'setting':
                this.setState({ showSetting: true, setting: data });
                break;
            case 'report':
                this.fetchApiReportLogs(data);
                break;
            default:
        }
    }

    deleteMonitoringSite = ({ ID }) => {
        axios.delete(`/api/v1/status/${ID}`).then(() => {
            this.componentDidMount();
        })
    }

    fetchApiReportLogs = ({ ID }) => {
        axios.get(`/api/v1/logs/${ID}`).then(({ data }) => {
            this.setState({ reportRows: data, showReport: true });
        })
    }

    render() {
        const { rows, showReport, reportRows, showLoading, setting, showSetting } = this.state;
        return (
            <React.Fragment>
                <ResponsiveContainer showTitle={true}>
                    <Segment inverted vertical style={{ padding: '5em 0em' }}>
                        <Container>
                            <Grid>
                                <Grid.Row>
                                    <Grid.Column>
                                        <Input label='http://' placeholder='mysite.com' onKeyPress={this.fetchUrlStatus} ref={this.setSearchRef}
                                            icon={<Icon name='search' inverted circular link onClick={this.fetchUrlStatus} />} fluid focus />
                                    </Grid.Column>
                                </Grid.Row>
                                <Grid.Row>
                                    <Grid.Column>
                                        <Dimmer active={showLoading}>
                                            <Loader size='large'></Loader>
                                        </Dimmer>
                                        <MonitoringList rows={rows} action={this.handleActions}></MonitoringList>
                                    </Grid.Column>
                                </Grid.Row>
                            </Grid>
                        </Container>
                    </Segment>
                    <Segment inverted vertical style={{ padding: '5em 0em' }}>
                        <Container>
                            <Grid divided inverted stackable>
                                <Grid.Row>
                                    <Grid.Column width={3}>
                                        <Header inverted as='h4' content='About' />
                                        <List link inverted>
                                            <List.Item as='a'>Sitemap</List.Item>
                                            <List.Item as='a'>Contact Us</List.Item>
                                            <List.Item as='a'>Religious Ceremonies</List.Item>
                                            <List.Item as='a'>Gazebo Plans</List.Item>
                                        </List>
                                    </Grid.Column>
                                </Grid.Row>
                            </Grid>
                        </Container>
                    </Segment>
                </ResponsiveContainer>
                <MonitoringReport visible={showReport} close={this.onHide} rows={reportRows}></MonitoringReport>
                <MonitoringSetting visible={showSetting} close={this.onHideSetting} data={setting}></MonitoringSetting>
            </React.Fragment>
        )
    }
}