export const reportHeading = [{
    label: 'StartTime',
    key: 'StartTime'
}, {
    label: 'EndTime',
    key: 'EndTime'
}, {
    label: 'TimeTime',
    key: 'TimeTakenInSeconds'
}, {
    label: 'Status',
    key: 'Status'
}];