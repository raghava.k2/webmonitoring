import React from 'react';
import { Segment, Sidebar, Grid, Table, Button } from 'semantic-ui-react';
import { reportHeading } from './monitoringReportConstants';

export class MonitoringReport extends React.Component {
    render() {
        const { visible, close, rows } = this.props;
        const display = visible ? 'block' : 'none';
        return (
            <Sidebar.Pushable as={Segment} style={{ display }}>
                <Sidebar animation='overlay'
                    icon='labeled'
                    vertical="true"
                    direction='right'
                    visible={visible}
                    width='very wide'
                    style={{ width: 650 }}>
                    <Grid>
                        <Grid.Row columns='2'>
                            <Grid.Column>
                                <Button icon='left arrow' onClick={close} basic>
                                </Button>
                            </Grid.Column>
                            <Grid.Column>
                                <h2>Report</h2>
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                            <Grid.Column>
                                <Table color='orange'>
                                    <Table.Header>
                                        <Table.Row>
                                            {reportHeading.map((head, index) => (
                                                <Table.HeaderCell key={index}>{head.label}</Table.HeaderCell>
                                            ))}
                                        </Table.Row>
                                    </Table.Header>
                                    <Table.Body>
                                        {rows.map((api, rowIndex) => {
                                            return (
                                                <Table.Row key={rowIndex}>
                                                    {reportHeading.map((head, cellIndex) => {
                                                        return (
                                                            <Table.Cell key={cellIndex + rowIndex}>{api[head.key]}</Table.Cell>
                                                        )
                                                    })}
                                                </Table.Row>
                                            )
                                        })
                                        }
                                    </Table.Body>
                                </Table>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Sidebar>
            </Sidebar.Pushable >
        )
    }
}