import React from 'react';
import { ResponsiveContainer } from '../NavBar/navBar'
import {
    Header,
    Container,
    Segment,
    Grid,
    List,
    Button,
    Icon
} from 'semantic-ui-react';
import './homePage.scss';
import { useHistory } from "react-router-dom";
export const HomePage = () => {
    let history = useHistory()
    const navigateTo = () => {
        history.push('/monitor')
    }
    return (
        <ResponsiveContainer showTitle={true}>
            <Segment inverted vertical style={{ padding: '5em 0em' }}>
                <Container text>
                    <Header
                        as='h1'
                        content='Imagine-a-Company'
                        inverted
                        style={{
                            fontSize: '4em',
                            fontWeight: 'normal',
                            marginBottom: 0,
                            marginTop: '3em',
                        }}
                    />
                    <Header
                        as='h2'
                        content='Want to monitor your Company APIs'
                        inverted
                        style={{
                            fontSize: '1.7em',
                            fontWeight: 'normal',
                            marginTop: '1.5em',
                        }}
                    />
                    <Button primary size='huge' onClick={navigateTo}>
                        Get Started
                              <Icon name='right arrow' />
                    </Button>
                </Container>
            </Segment>
            <Segment inverted vertical style={{ padding: '5em 0em' }}>
                <Container>
                    <Grid divided inverted stackable>
                        <Grid.Row>
                            <Grid.Column width={3}>
                                <Header inverted as='h4' content='About' />
                                <List link inverted>
                                    <List.Item as='a'>Sitemap</List.Item>
                                    <List.Item as='a'>Contact Us</List.Item>
                                    <List.Item as='a'>Religious Ceremonies</List.Item>
                                    <List.Item as='a'>Gazebo Plans</List.Item>
                                </List>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Container>
            </Segment>
        </ResponsiveContainer >
    );
}