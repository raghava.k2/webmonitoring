import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import './registration.scss';
import { Button, Form, Grid, Header, Message, Segment, Dropdown } from 'semantic-ui-react'

export class Registration extends Component {
    constructor(props) {
        super(props);
        this.state = {
            registrationFrom: {},
            gender: [
                { key: 'm', text: 'Male', value: 'male' },
                { key: 'f', text: 'Female', value: 'female' },
                { key: 'o', text: 'Other', value: 'other' },
            ]
        }
    }
    setFormDetails(key, e) {
        console.log('key', e, e.currentTarget.value, this.state.registrationFrom)
    }
    render() {
        return (
            <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
                <Grid.Column style={{ maxWidth: 450 }}>
                    <Header as='h2' color='teal' textAlign='center'>
                        Register your account
                    </Header>
                    <Form size='large'>
                        <Segment stacked>
                            <Form.Input fluid icon='user' iconPosition='left' placeholder='User Name' type='text'
                                value={this.state.registrationFrom.userName}
                                onClick={e => this.setFormDetails('userName', e)} />
                            <Form.Input
                                fluid
                                icon='lock'
                                iconPosition='left'
                                placeholder='Password'
                                type='password'
                                value={this.state.registrationFrom.password}
                                onClick={e => this.setFormDetails('password', e)}
                            />
                            <Form.Input
                                fluid
                                icon='lock'
                                iconPosition='left'
                                placeholder='Confirm Password'
                                type='password'
                                value={this.state.registrationFrom.confirmPassword}
                                onClick={e => this.setFormDetails('confirmPassword', e)}
                            />
                            <Dropdown button
                                className='field ui fluid left icon r-dropdown'
                                floating
                                labeled
                                icon='filter'
                                options={this.state.gender}
                                search
                                selection
                                value={this.state.registrationFrom.gender}
                                onChange={e => this.setFormDetails('gender', e)} />
                            <Form.Input
                                fluid
                                icon='mail'
                                iconPosition='left'
                                placeholder='Email Id'
                                type='email'
                                value={this.state.registrationFrom.mail}
                                onClick={e => this.setFormDetails('mail', e)}
                            />
                            <Button color='teal' fluid size='large'>
                                Register
                            </Button>
                        </Segment>
                    </Form>
                    <Message>
                        Already have a Account? <Link to='/login'>Login</Link>
                    </Message>
                </Grid.Column>
            </Grid>
        );
    }
}