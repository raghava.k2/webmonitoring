import React, { Component } from 'react';
import { Provider } from 'react-redux'
import { BrowserRouter as Router } from 'react-router-dom'
import { routes } from '../config/routes'
import { RouteWithSubRoutes } from '../utils';
import { store } from '../store'
export class Root extends Component {
    render() {
        return (
            <Provider store={store}>
                <Router>
                    {routes.map((item, index) => (
                        <RouteWithSubRoutes key={index} {...item} />
                    ))}
                </Router>
            </Provider>
        );
    }
}