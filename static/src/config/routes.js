import { HomePage } from "../components/HomePage/homePage";
import { Login } from "../components/Login/login";
import { Registration } from "../components/Registration/registration";
import { Monitor } from "../components/Monitor/monitor";
import { Redirect } from "react-router-dom";
import React from 'react';

const CustomRedirect = () => (
    <Redirect to='/home' />
);

export const routes = [{
    path: '/',
    component: CustomRedirect
}, {
    path: '/home',
    component: HomePage
}, {
    path: '/login',
    component: Login
}, {
    path: '/register',
    component: Registration
}, {
    path: '/monitor',
    component: Monitor
}];