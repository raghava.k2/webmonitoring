# Web Monitoring
This project helps us to monitor the apis/sites.

## Prerequisite
- Install go lang > 1.11 software in your system
- Install mygwin-64 as we are using embedded sqlite3 which requires gcc
- Install Node.js version > 10

## Installation Steps
- Install the above software and they need to be there in your system Path
- Now in the root directory run `go mod download`
- Go to static directory and run `npm install` / `yarn install`
- The above two steps will download the required dependencies

## Running in dev mode
- Go to root directory and run `go run cmd\httpd\main.go` which runs at [http://localhost:23546/](http://localhost:23546/)
- Now in seperate terminal go to statis directory and run `npm run start`/ `yarn start`
- Now open a browser and click [http://localhost:3000](http://localhost:3000)

## Building the project
- set environment variable `GIN_MODE=release` before building the app.
- To build Static content go to static directory path and run `yarn build` / `npm run build`. This will create a build folder under static directory
- Go to root directory path and run `go build cmd\httpd\main.go`. This will build binary file at root directory with name `main.exe`
- Now open any command prompt / terminal and run `main.exe`.
- Now open any browser and copy/click [http://localhost:23546/](http://localhost:23546/)
