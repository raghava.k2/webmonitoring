package main

import (
	"os"
	"strings"
	"webmonitor/cmd/models"
	"webmonitor/cmd/scheduler"
	"webmonitor/cmd/service"

	"github.com/gin-contrib/static"
	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	models.InitDB()
	scheduler.InitScheduler()
	setStaticPath(r)
	r.GET("/", service.Index)
	v1 := r.Group("/api/v1")
	{
		v1.POST("/status", service.Status)
		v1.PUT("/status/:id", service.UpdateSiteDetails)
		v1.DELETE("/status/:id", service.DeleteSite)
		v1.GET("/status", service.GetSitesList)
		v1.GET("/logs", service.GetAllLogs)
		v1.GET("/logs/:id", service.GetLogsByID)
	}
	r.Run(":23546")
}

func setStaticPath(r *gin.Engine) {
	profile := os.Getenv("GIN_MODE")
	if strings.Compare(profile, "release") == 0 {
		r.LoadHTMLFiles("./bin/build/index.html")
		r.Use(static.Serve("/", static.LocalFile("./bin/build/", true)))
	}
}
