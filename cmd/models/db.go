package models

import (
	"log"
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	_ "github.com/mattn/go-sqlite3"
)

/*DB Object*/
var db *gorm.DB

/*InitDB connection*/
func InitDB() {
	var err error
	err = os.Remove("./data.db")
	if err != nil {
		log.Println("unable to delete data.db")
	}
	// Openning file
	db, err = gorm.Open("sqlite3", "./data.db")
	// Error
	if err != nil {
		panic(err)
	}
	// Display SQL queries
	db.LogMode(true)
	createTables(db)
}

func createTables(db *gorm.DB) {
	db.CreateTable(&WebSite{})
	db.CreateTable(&WebLog{})
}
