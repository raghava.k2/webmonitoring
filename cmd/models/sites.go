package models

import (
	"github.com/jinzhu/gorm"
)

/*WebSite Entity*/
type WebSite struct {
	gorm.Model
	URL                string
	ReqTime            string
	ResTime            string
	TimeTaken          string
	IntervalTime       uint
	IntervalTimeFormat string
	WebLog             []WebLog
}

func (w *WebSite) Create() {
	db.Create(w)
}

func (w *WebSite) FindAll() []WebSite {
	var records []WebSite
	db.Find(&records)
	return records
}

func (w *WebSite) FindByID(ID uint) *WebSite {
	db.First(w, ID)
	return w
}

func (w *WebSite) Update() {
	db.Save(w)
}

func (w *WebSite) Delete() {
	db.Unscoped().Delete(w)
}
