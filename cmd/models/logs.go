package models

import "github.com/jinzhu/gorm"

/*Log Table*/
type WebLog struct {
	gorm.Model
	StartTime          string
	EndTime            string
	TimeTakenInSeconds string
	Status             string
	WebSiteRefer       uint
}

func (l *WebLog) Create() {
	db.Create(l)
}

func (l *WebLog) FindByWebSiteID(ID uint) []WebLog {
	var records []WebLog
	db.Where(&WebLog{WebSiteRefer: ID}).Find(&records)
	return records
}

func (l *WebLog) FindAll() []WebLog {
	var records []WebLog
	db.Find(&records)
	return records
}

func (l *WebLog) DeleteByID(ID uint) {
	db.Unscoped().Delete(&WebLog{WebSiteRefer: ID})
}
