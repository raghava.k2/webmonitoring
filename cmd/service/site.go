package service

import (
	"errors"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"time"
	"webmonitor/cmd/models"
	"webmonitor/cmd/scheduler"

	"github.com/gin-gonic/gin"
)

const TIME_FORMAT = "01/02/06 15:04:05"

type SiteDTO struct {
	URL          string
	IntervalTime uint
	TimeFormat   string
}

type UrlTriggerDetail struct {
	ReqTime   string
	ResTime   string
	TimeTaken string
}

/*Index function*/
func Index(c *gin.Context) {
	c.HTML(http.StatusOK, "index.html", map[string]string{})
}

/*Status function*/
func Status(c *gin.Context) {
	var siteDto SiteDTO
	err := c.BindJSON(&siteDto)
	log.Println("site DTO : ", siteDto)
	if err != nil {
		log.Println(err)
		c.AbortWithStatusJSON(400, map[string]interface{}{"exception": "Invalid Data Format"})
	} else {
		v, err := triggerCall(siteDto.URL)
		if err != nil {
			log.Println(err)
			c.AbortWithStatusJSON(400, map[string]interface{}{"exception": err})
		} else {
			w := models.WebSite{URL: siteDto.URL,
				ReqTime:   v.ReqTime,
				ResTime:   v.ResTime,
				TimeTaken: v.TimeTaken}
			w.Create()
			c.JSON(200, w)
		}
	}
}

func GetSitesList(c *gin.Context) {
	w := models.WebSite{}
	c.JSON(200, w.FindAll())
}

func UpdateSiteDetails(c *gin.Context) {
	var siteDto SiteDTO
	err := c.BindJSON(&siteDto)
	if err != nil {
		c.AbortWithStatusJSON(400, map[string]interface{}{"exception": "Invalid Json Format"})
	} else {
		v, err := triggerCall(siteDto.URL)
		if err != nil {
			log.Println(err)
			c.AbortWithStatusJSON(400, map[string]interface{}{"exception": err})
		} else {
			id, _ := strconv.Atoi(c.Param("id"))
			w := &models.WebSite{}
			w.FindByID(uint(id))
			w.URL = siteDto.URL
			w.ReqTime = v.ReqTime
			w.ResTime = v.ResTime
			w.TimeTaken = v.TimeTaken
			w.IntervalTime = siteDto.IntervalTime
			w.IntervalTimeFormat = siteDto.TimeFormat
			w.Update()
			scheduler.StoreJob(w)
		}
	}
}

func DeleteSite(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))
	w := &models.WebSite{}
	l := &models.WebLog{}
	w.FindByID(uint(id))
	w.Delete()
	l.DeleteByID(uint(id))
	scheduler.DeleteJob(uint(id))
}

func triggerCall(url string) (UrlTriggerDetail, error) {
	requestTime := time.Now()
	resp, err := http.Get(url)
	if err != nil {
		return UrlTriggerDetail{}, errors.New(err.Error())
	}
	responseTime := time.Now()
	timeTaken := responseTime.Sub(requestTime)
	defer resp.Body.Close()
	_, error := ioutil.ReadAll(resp.Body)
	if error != nil {
		return UrlTriggerDetail{}, errors.New("Unable to get reponse body")
	}
	return UrlTriggerDetail{ReqTime: requestTime.Format(TIME_FORMAT),
		ResTime:   responseTime.Format(TIME_FORMAT),
		TimeTaken: timeTaken.String()}, nil
}
