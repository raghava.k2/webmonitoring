package service

import (
	"strconv"
	"webmonitor/cmd/models"

	"github.com/gin-gonic/gin"
)

func GetLogsByID(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))
	l := &models.WebLog{}
	c.JSON(200, l.FindByWebSiteID(uint(id)))
}

func GetAllLogs(c *gin.Context) {
	l := &models.WebLog{}
	c.JSON(200, l.FindAll())
}
