package scheduler

import (
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"time"
	"webmonitor/cmd/models"
)

const TIME_FORMAT = "01/02/06 15:04:05"

type CustomJob struct {
}

func (c CustomJob) Execute(data interface{}) error {
	log.Println("inside execute method")
	requestTime := time.Now()
	w := data.(*models.WebSite)
	resp, err := http.Get(w.URL)
	responseTime := time.Now()
	timeTaken := responseTime.Sub(requestTime)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	_, error := ioutil.ReadAll(resp.Body)
	if error != nil {
		saveErrorLog(requestTime, "Down", w.ID)
		return error
	}
	saveLog(requestTime, responseTime, timeTaken, "Up", w.ID)
	return nil
}

func saveErrorLog(requestTime time.Time, status string, siteID uint) {
	l := models.WebLog{StartTime: requestTime.Format(TIME_FORMAT),
		EndTime:            "",
		TimeTakenInSeconds: "",
		Status:             status,
		WebSiteRefer:       siteID}
	l.Create()
}

func saveLog(requestTime time.Time, responseTime time.Time, timeTaken time.Duration, status string, siteID uint) {
	l := models.WebLog{StartTime: requestTime.Format(TIME_FORMAT),
		EndTime:            responseTime.Format(TIME_FORMAT),
		TimeTakenInSeconds: timeTaken.String(),
		Status:             status,
		WebSiteRefer:       siteID}
	l.Create()
}

func StoreJob(data interface{}) {
	w := data.(*models.WebSite)
	var jobDetail *JobDetail = &JobDetail{}
	var job Job = CustomJob{}
	jobDetail.Name = "job" + strconv.Itoa(int(w.ID))
	jobDetail.CheckInterval = w.IntervalTime
	jobDetail.IntervalFormat = w.IntervalTimeFormat
	jobDetail.Data = data
	jobDetail.JobStruct = job
	scheduler.AddJob(jobDetail)
}

func DeleteJob(ID uint) {
	jobName := "job" + strconv.Itoa(int(ID))
	scheduler.DeleteJob(jobName)
}
