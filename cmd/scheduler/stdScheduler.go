package scheduler

import (
	"errors"
	"strings"
	"sync"
	"time"
)

var scheduler *StdScheduler

type Job interface {
	Execute(data interface{}) error
}

type JobDetail struct {
	Name           string
	Data           interface{}
	CheckInterval  uint
	IntervalFormat string
	JobStruct      Job
}

type JobMap struct {
	JobDetails *JobDetail
	Tick       *time.Ticker
}

type StdScheduler struct {
	lock   sync.Mutex
	jobMap map[string]*JobMap
}

func (s *StdScheduler) Start() {
	s.jobMap = make(map[string]*JobMap)
}

func (s *StdScheduler) AddJob(jobDetail *JobDetail) {
	s.DeleteJob(jobDetail.Name)
	s.lock.Lock()
	defer s.lock.Unlock()
	s.jobMap[jobDetail.Name] = &JobMap{JobDetails: jobDetail, Tick: startTheJob(jobDetail)}
}

func (s *StdScheduler) DeleteJob(jobName string) error {
	s.lock.Lock()
	defer s.lock.Unlock()
	value, ok := s.jobMap[jobName]
	if !ok {
		return errors.New("Error deleteing the job")
	}
	jobMap := value
	jobMap.Tick.Stop()
	delete(s.jobMap, jobName)
	return nil
}

func InitScheduler() {
	scheduler = &StdScheduler{}
	scheduler.Start()
}

func startTheJob(jobDetail *JobDetail) *time.Ticker {
	tick := time.NewTicker(getIntervalTime(jobDetail))
	go func() {
		for {
			select {
			case <-tick.C:
				var j Job = jobDetail.JobStruct
				j.Execute(jobDetail.Data)
			}
		}
	}()
	return tick
}

func getIntervalTime(jobDetail *JobDetail) time.Duration {
	if strings.Compare(jobDetail.IntervalFormat, "M") == 0 {
		return time.Duration(jobDetail.CheckInterval) * time.Minute
	}
	return time.Duration(jobDetail.CheckInterval) * time.Second
}
